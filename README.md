<h1 align='center'> Nomes mais comuns no Brasil </h1>
<p align='justify'> Aplicação que mostra o ranking de nomes mais comuns no país, podendo ser fornecido baseado no estado, cidade ou por década. </p>

### Principais funcionalidades da aplicação

- Menu de opções
  - Ao acessar a aplicação, um menu de 5 opções será exibido para escolha do usuário.
- Opção 1: Pesquisar nomes mais comuns por unidade federativa
  - Para a unidade federativa escolhida pelo usuário, fornece três tabelas para o nomes mais comuns: ranking geral de nomes, ranking de nomes masculinos e ranking de nomes femininos, com as respectivas frequências e percentagens em relação ao total da população da unidade federativa.
- Opção 2: Pesquisar nomes mais comuns por cidade
  - Para a cidade escolhida pelo usuário, fornece três tabelas para o nomes mais comuns: ranking geral de nomes, ranking de nomes masculinos e ranking de nomes femininos, com as respectivas frequências e percentagens em relação ao total da população da cidade.
- Opção 3: Pesquisar frequência de nomes específicos por década
  - Para os nomes escolhidos pelo usuário, fornece uma tabela que mostra a frequência deles por década, do ano 1930 até 2010.
- Opção 4: Salvar ranking de nomes no banco de dados
  - Para unidade federativa escolhida pelo usuário, salva todas suas cidades e o ranking de nomes de cada uma delas no banco de dados, de forma assíncrona a partir da execução do sidekiq.
- Opção 5: Sair
  - Finaliza a execução da aplicação.

> Status do Projeto: Em desenvolvimento

### Instalações necessárias para aplicação

- [Ruby](https://www.ruby-lang.org/en/documentation/installation/): versão 2.7.1
- [Docker](https://docs.docker.com/engine/install/)

### Gems utilizadas

- [colorize](https://github.com/fazibear/colorize) - customiza cor do output no terminal
- [faraday](https://lostisland.github.io/faraday/) - realiza requisições nas apis
- [pg](https://github.com/ged/ruby-pg) - interface do Ruby para o banco de dados PotsgreSQL
- [rspec](https://github.com/rspec/rspec) - utilizado para testes da aplicação
- [rubocop](https://github.com/rubocop-hq/rubocop) - analiza a legibilidade e boas práticas do código
- [sidekiq](https://github.com/mperham/sidekiq) - permite realizar tarefas assíncronas na aplicação

### Imagens utilizada no docker

-[postgres](https://hub.docker.com/_/postgres) - versão 13.0
-[redis](https://hub.docker.com/_/redis) - versão 6.0.9
-[ruby](https://hub.docker.com/_/ruby) - versão 2.7.1

### Como baixar aplicação e preparar para uso

<p> Clonar repositório: git clone https://gitlab.com/eduardo_gris/names_frequency.git </p>
<p> Entrar na pasta do projeto: cd names_frequency </p>
<p> Iniciar a criação do docker-compose: docker-compose build </p>
<p> Abrir a aplicação pelo docker: docker-compose run app bash </p>
<p> Criar tabelas do banco de dados: bin/setup </p>

### Execução da aplicação

<p> Abrir a aplicação pelo docker: docker-compose run app bash </p>
<p> Começar a execução da aplicação: ruby lib/app.rb </p>

### Execução do Sidekiq

<p> Abrir a aplicação pelo docker: docker-compose run app bash </p>
<p> Começar a execução do sidekiq: bundle exec sidekiq -r ./lib/sidekiq_connection.rb </p>

### Execução de testes e da análise do código

<p> Abrir a aplicação pelo docker: docker-compose run app bash </p>
<p> Começar a execução dos testes: rspec </p>
<p> Começar a análise do código: rubocop </p>
