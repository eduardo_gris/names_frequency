FROM ruby:2.7.1

RUN apt-get update
RUN mkdir names_frequency
COPY create-multiple-postgresql-databases.sh /docker-entrypoint-initdb.d/

WORKDIR /names_frequency
RUN gem install bundler:2.1.4
ADD Gemfile* /names_frequency/
RUN bundle install
ADD . /names_frequency