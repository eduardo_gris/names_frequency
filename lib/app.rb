require 'sidekiq'

Sidekiq.configure_server do |config|
  config.redis = { url: 'redis://redis:6379/0' }
end

Sidekiq.configure_client do |config|
  config.redis = { url: 'redis://redis:6379/0' }
end

class App
  require 'bundler'
  Bundler.require(:default)

  PROJECT_ROOT = File.expand_path('..', __dir__)
  Dir.glob(File.join(PROJECT_ROOT, 'lib', 'clients', '*.rb')).each { |file| require file }
  Dir.glob(File.join(PROJECT_ROOT, 'lib', 'controllers', '*.rb')).each { |file| require file }
  Dir.glob(File.join(PROJECT_ROOT, 'lib', 'jobs', '*.rb')).each { |file| require file }
  Dir.glob(File.join(PROJECT_ROOT, 'lib', 'models', '*.rb')).each { |file| require file }
  Dir.glob(File.join(PROJECT_ROOT, 'lib', 'repositories', '*.rb')).each { |file| require file }
  Dir.glob(File.join(PROJECT_ROOT, 'lib', 'views', '**', '*.rb')).each { |file| require file }

  loop do
    puts ERB.new(Option.option_list).result(binding)
    option = gets.chomp
    case option
    when '1'
      FederativeUnitsController.federative_unit_execution
    when '2'
      CitiesController.city_execution
    when '3'
      NameFrequenciesController.names_frequency_execution
    when '4'
      RankingNamesController.ranking_name_execution
    when '5'
      break
    else
      puts Error.incorrect_option
    end
  end
end
