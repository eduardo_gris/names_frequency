class NameFrequency
  attr_reader :name, :period, :frequency

  def initialize(name:, period:, frequency:)
    @name = name
    @period = period
    @frequency = frequency
  end

  def self.period_format(period)
    period.delete_prefix('[').chop.gsub(',', '-')
  end

  def self.all_decade_name_frequencies(name)
    response = IbgeApiClient.decade_name_frequencies_api(name)
    list = json_parse(response)
    decade_name_frequencies_array(list, name)
  end

  def self.json_parse(response)
    JSON.parse(response.body, symbolize_names: true)
  end

  def self.decade_name_frequencies_array(list, name)
    decade_name_frequencies = []
    if list != []
      list[0][:res].drop(1).each do |d_f|
        decade_name_frequencies << NameFrequency.new(name: name.upcase, period: period_format(d_f[:periodo]),
                                                     frequency: d_f[:frequencia])
      end
    end
    decade_name_frequencies
  end
end
