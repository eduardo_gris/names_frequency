class City
  attr_reader :city_id, :name

  def initialize(city_id:, name:)
    @city_id = city_id
    @name = name
  end

  def self.all_uf_cities(fu_id)
    response = IbgeApiClient.fu_cities_api(fu_id)
    list = json_parse(response)
    uf_cities_array(list)
  end

  def self.json_parse(response)
    JSON.parse(response.body, symbolize_names: true)
  end

  def self.uf_cities_array(list)
    uf_cities = []
    list.each { |city| uf_cities << City.new(city_id: city[:id], name: city[:nome]) }
    uf_cities
  end
end
