class RankingName
  require_relative 'federative_unit'

  attr_reader :sex, :ranking, :name, :frequency, :local_id

  def initialize(sex:, ranking:, name:, frequency:, local_id:)
    @sex = sex
    @ranking = ranking
    @name = name
    @frequency = frequency
    @local_id = local_id
  end

  def self.general_rank_names(local_id)
    response = IbgeApiClient.general_rank_names_api(local_id)
    list = json_parse(response)
    rank_names_array(list, 'G', local_id)
  end

  def self.woman_rank_names(local_id)
    response = IbgeApiClient.woman_rank_names_api(local_id)
    list = json_parse(response)
    rank_names_array(list, 'W', local_id)
  end

  def self.man_rank_names(local_id)
    response = IbgeApiClient.man_rank_names_api(local_id)
    list = json_parse(response)
    rank_names_array(list, 'M', local_id)
  end

  def self.json_parse(response)
    JSON.parse(response.body, symbolize_names: true)
  end

  def self.rank_names_array(list, sex, local_id)
    rank_names = []
    list[0][:res].each do |rank_info|
      rank_names << RankingName.new(sex: sex.to_s, ranking: rank_info[:ranking], name: rank_info[:nome],
                                    frequency: rank_info[:frequencia], local_id: local_id)
    end
    rank_names
  end
end
