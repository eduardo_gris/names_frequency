class FederativeUnit
  attr_reader :fu_id, :name, :abbreviation

  def initialize(fu_id:, name:, abbreviation:)
    @fu_id = fu_id
    @name = name
    @abbreviation = abbreviation
  end

  def self.all_federative_units
    response = IbgeApiClient.federative_unit_api
    list = json_parse(response)
    federative_units_array(list)
  end

  def self.json_parse(response)
    JSON.parse(response.body, symbolize_names: true)
  end

  def self.federative_units_array(list)
    federative_units = []
    list.each { |fu| federative_units << FederativeUnit.new(fu_id: fu[:id], name: fu[:nome], abbreviation: fu[:sigla]) }
    federative_units
  end
end
