class Success
  def self.success_table
    'Tabelas com ranking de nomes acima'.green
  end

  def self.success_names_table
    "\nTabela de frequência de nomes por década acima".green
  end

  def self.success_save
    %(<%= city.name %> salvo com sucesso).green
  end
end
