class Option
  def self.option_list
    %(\n-----------------------------------------
RANKING DOS NOMES MAIS COMUNS NO PAÍS

[1] Pesquisar nomes mais comuns por unidade federativa
[2] Pesquisar nomes mais comuns por cidade
[3] Pesquisar frequência de nomes específicos por década
[4] Salvar ranking de nomes no banco de dados
[5] Sair
-----------------------------------------
Escolha a opção de pesquisa:
)
  end
end
