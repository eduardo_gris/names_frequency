class Error
  def self.incorrect_requisition
    %(
Requisição incorreta).red
  end

  def self.incorrect_option
    'Opção inválida'.red
  end

  def self.saved_before
    %(<%= city.name %> já está salvo).red
  end
end
