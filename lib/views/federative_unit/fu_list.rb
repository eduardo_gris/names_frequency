class FuList
  def self.view_list
    %(
UNIDADES FEDERATIVAS:

<% federative_units.each do |unit| %>
  \033[F<%= unit.name %> - <%= unit.abbreviation %>
<% end %>
Acima estão todas as unidades federativas com suas siglas.
Digite a sigla da unidade federativa escolhida:
)
  end
end
