class NamesTable
  def self.title_base(names_frequency)
    base = '|PERÍODO/NOME|'
    names_frequency.each do |names_info|
      base += TableStructure.result_view(names_info[0].name.length, names_info[0].name)
    end
    base
  end

  def self.names_title
    %(
FREQUÊNCIA DE NOMES POR DÉCADA:

<%= '-------------' * (names_frequency.length + 1) %>-
<%= NamesTable.title_base(names_frequency) %>
<%= '-------------' * (names_frequency.length + 1) %>-
\033[F\033[F
)
  end

  def self.names_content
    %(<% j = 0 %>
<% names_frequency[0].each do |info| %>
  <% content_view = '|' + TableStructure.result_view(info.period.length, info.period) %>
  <% names_frequency.each do |name| %> \033[F\033[F
    <% content_view += TableStructure.result_view(name[j].frequency.to_s.length, name[j].frequency.to_s) %>
  <% end %>
  <% j += 1 %>
  \033[F\033[F\033[F\033[F<%= content_view %>
<%= '-------------' * (names_frequency.length + 1) %>-
<% end %>)
  end
end
