class GeneralTable
  require_relative 'table_structure'

  def self.general_title
    %(
RANK GERAL DOS NOMES MAIS COMUNS:
<%= TableStructure.title_table %>)
  end

  def self.view_general_table
    %(<% ERB.new(GeneralTable.general_title).result(binding) %>
<% general_rank_names.each do |result| %>
\033[F<%= TableStructure.table_content_percentage(result) %>
-----------------------------------------------------
<% end %>
)
  end
end
