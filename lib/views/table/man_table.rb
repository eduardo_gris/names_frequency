class ManTable
  require_relative 'table_structure'
  require_relative '../success_message/success'

  def self.man_title
    %(
RANK DOS NOMES MASCULINOS MAIS COMUNS:
<%= TableStructure.title_table %>)
  end

  def self.view_man_table
    %(<% ERB.new(ManTable.man_title).result(binding) %>
<% man_rank_names.each do |result| %>
\033[F<%= TableStructure.table_content_percentage(result) %>
-----------------------------------------------------
<% end %>
<%= Success.success_table %>
)
  end
end
