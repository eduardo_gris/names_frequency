class TableStructure
  def self.title_table
    %(
-----------------------------------------------------
|    Nome    |  Ranking   | Frequência | Percentagem|
----------------------------------------------------- )
  end

  def self.result_view(length, result)
    white_space = (12 - length) / 2
    if (length % 2).zero?
      "#{' ' * white_space}#{result}#{' ' * white_space}|"
    else
      "#{' ' * (white_space + 1)}#{result}#{' ' * white_space}|"
    end
  end

  def self.table_content(result)
    result_name = result_view(result.name.length, result.name)
    result_ranking = result_view(result.ranking.to_s.length, result.ranking)
    result_frequency = result_view(result.frequency.to_s.length, result.frequency)
    "|#{result_name}#{result_ranking}#{result_frequency}"
  end

  def self.table_content_percentage(result)
    content = TableStructure.table_content(result)
    percentage = PopulationsController.get_population_percentage(result.frequency.to_i, result.local_id)
    result_percentage = result_view(percentage.length, percentage)
    "#{content}#{result_percentage}"
  end
end
