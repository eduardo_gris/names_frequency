class WomanTable
  require_relative 'table_structure'

  def self.woman_title
    %(
RANK DOS NOMES FEMININOS MAIS COMUNS:
<%= TableStructure.title_table %>)
  end

  def self.view_woman_table
    %(<% ERB.new(WomanTable.woman_title).result(binding) %>
<% woman_rank_names.each do |result| %>
\033[F<%= TableStructure.table_content_percentage(result) %>
-----------------------------------------------------
<% end %>
)
  end
end
