class FederativeUnitsJob
  include Sidekiq::Worker

  def perform
    response = IbgeApiClient.federative_unit_api
    list = FederativeUnitsController.json_parse(response)
    FederativeUnitsController.save_federative_units(list)
  end
end
