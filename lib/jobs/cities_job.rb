class CitiesJob
  include Sidekiq::Worker

  def perform(city_id, name)
    CityRepository.new.save(city_id, name) if CityRepository.new.count_city(city_id) == '0'
    RankingNamesController.create(city_id)
  end
end
