class NameFrequenciesController
  def self.names_frequency_execution
    names = NameFrequenciesController.names_choose_format
    names_frequency = NameFrequenciesController.names_frequency_info(names)
    NameFrequenciesController.names_table(names_frequency)
  end

  def self.names_choose_format
    puts NameFrequencyView.name_frequency_title
    gets.chomp.delete(' ').split(',').reject(&:empty?)
  end

  def self.names_frequency_info(names)
    names_frequency_info = []
    names.each do |name|
      if NameFrequency.all_decade_name_frequencies(name) != []
        names_frequency_info << NameFrequency.all_decade_name_frequencies(name)
      end
    end
    names_frequency_info
  end

  def self.names_table(names_frequency)
    if names_frequency != []
      puts ERB.new(NamesTable.names_title).result(binding)
      puts ERB.new(NamesTable.names_content).result(binding)
      puts Success.success_names_table
    else
      puts Error.incorrect_requisition
    end
  end
end
