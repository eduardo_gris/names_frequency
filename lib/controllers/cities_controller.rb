class CitiesController
  require 'erb'

  def self.city_execution
    fu_id = FederativeUnitsController.federative_unit_choose
    city_id = CitiesController.city_choose(fu_id)
    RankingNamesController.ranking_tables(city_id)
  end

  def self.city_choose(fu_id)
    cities = City.all_uf_cities(fu_id)
    puts ERB.new(CityView.city_title).result(binding)
    city_name = gets.chomp
    city_id = ''
    cities.each { |city| city_id = city.city_id if city.name == city_name }
    city_id
  end

  def self.create(fu_id)
    response = IbgeApiClient.fu_cities_api(fu_id)
    list = CitiesController.json_parse(response)
    CitiesController.save_cities(list)
  end

  def self.json_parse(response)
    JSON.parse(response.body, symbolize_names: true)
  end

  def self.save_cities(list)
    list.each do |city|
      CitiesJob.perform_async(city[:id], city[:nome])
    end
  end
end
