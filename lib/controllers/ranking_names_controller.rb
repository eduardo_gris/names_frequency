class RankingNamesController
  def self.ranking_name_execution
    federative_units = FederativeUnit.all_federative_units
    puts ERB.new(FuList.view_list).result(binding)
    FederativeUnitsJob.perform_async
    fu_id = gets.chomp
    CitiesController.create(fu_id)
  end

  def self.where_change_format(results)
    formated_results = []
    results.each do |result|
      data = result.transform_keys!(&:to_sym)
      formated_results << RankingName.new(sex: data[:sex], ranking: data[:ranking],
                                          name: data[:name], frequency: data[:frequency], local_id: data[:local_id])
    end
    formated_results
  end

  def self.more_than_one_year?(results)
    Date.today - 365 > Date.parse(results[0].transform_keys!(&:to_sym)[:created_date])
  end

  def self.general_rank_names(id)
    if RankingNamesController.database_different_total_quantity?('G', id)
      general_rank_names = RankingName.general_rank_names(id)
    else
      results = RankingNameRepository.new.where('G', id)
      results = RankingNamesController.general_update(id) if RankingNamesController.more_than_one_year?(results)
      general_rank_names = RankingNamesController.where_change_format(results)
    end
    puts ERB.new(GeneralTable.view_general_table).result(binding)
  end

  def self.woman_rank_names(id)
    if RankingNamesController.database_different_total_quantity?('W', id)
      woman_rank_names = RankingName.woman_rank_names(id)
    else
      results = RankingNameRepository.new.where('W', id)
      results = RankingNamesController.woman_update(id) if RankingNamesController.more_than_one_year?(results)
      woman_rank_names = RankingNamesController.where_change_format(results)
    end
    puts ERB.new(WomanTable.view_woman_table).result(binding)
  end

  def self.man_rank_names(id)
    if RankingNamesController.database_different_total_quantity?('M', id)
      man_rank_names = RankingName.man_rank_names(id)
    else
      results = RankingNameRepository.new.where('M', id)
      results = RankingNamesController.man_update(id) if RankingNamesController.more_than_one_year?(results)
      man_rank_names = RankingNamesController.where_change_format(results)
    end
    puts ERB.new(ManTable.view_man_table).result(binding)
  end

  def self.database_different_total_quantity?(sex, id)
    total_ranking_quantity = '20'
    RankingNameRepository.new.count_rankings_sex(sex, id) != total_ranking_quantity
  end

  def self.ranking_tables(id)
    if id != ''
      general_rank_names(id)
      woman_rank_names(id)
      man_rank_names(id)
    else
      puts Error.incorrect_requisition
    end
  end

  def self.general_create(id)
    RankingName.general_rank_names(id).each do |general_info|
      RankingNameRepository.new.save(general_info) if RankingNameRepository.new.count_ranking(general_info) == '0'
    end
  end

  def self.woman_create(id)
    RankingName.woman_rank_names(id).each do |woman_info|
      RankingNameRepository.new.save(woman_info) if RankingNameRepository.new.count_ranking(woman_info) == '0'
    end
  end

  def self.man_create(id)
    RankingName.man_rank_names(id).each do |man_info|
      RankingNameRepository.new.save(man_info) if RankingNameRepository.new.count_ranking(man_info) == '0'
    end
  end

  def self.create(id)
    RankingNamesController.general_create(id)
    RankingNamesController.woman_create(id)
    RankingNamesController.man_create(id)
  end

  def self.general_update(id)
    general_rank_names = RankingName.general_rank_names(id)
    RankingNamesController.rank_update(general_rank_names, 'G', id)
  end

  def self.woman_update(id)
    woman_rank_names = RankingName.woman_rank_names(id)
    RankingNamesController.rank_update(woman_rank_names, 'W', id)
  end

  def self.man_update(id)
    man_rank_names = RankingName.man_rank_names(id)
    RankingNamesController.rank_update(man_rank_names, 'M', id)
  end

  def self.rank_update(rank_names, sex, id)
    rank_names.each do |rank_info|
      RankingNameRepository.new.update(rank_info)
    end
    RankingNameRepository.new.where(sex, id)
  end
end
