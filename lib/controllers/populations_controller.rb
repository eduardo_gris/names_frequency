class PopulationsController
  require 'csv'
  def self.get_total_population(id)
    total_population = ''
    table = CSV.parse(File.read('lib/population_csv/populacao_2019.csv'), headers: true)
    table.each do |data|
      total_population = data['População Residente - 2019'] if data['Cód.'] == id.to_s
    end
    total_population.to_f
  end

  def self.comma_separator(percentage)
    percentage.to_s.gsub('.', ',')
  end

  def self.get_population_percentage(frequency, id)
    total_population = PopulationsController.get_total_population(id)
    percentage = frequency * 100 / total_population
    "#{PopulationsController.comma_separator(percentage.round(1))} %"
  end
end
