class FederativeUnitsController
  require 'erb'

  def self.federative_unit_execution
    fu_id = FederativeUnitsController.federative_unit_choose
    RankingNamesController.ranking_tables(fu_id)
  end

  def self.federative_unit_choose
    federative_units = FederativeUnitsController.federative_units
    puts ERB.new(FuList.view_list).result(binding)
    choose_abbreviation = gets.chomp
    fu_id = ''
    federative_units.each { |fu| fu_id = fu.fu_id if fu.abbreviation == choose_abbreviation }
    fu_id
  end

  def self.all_change_format(results)
    formatted_results = []
    results.each do |result|
      data = result.transform_keys!(&:to_sym)
      formatted_results << FederativeUnit.new(fu_id: data[:fu_id], name: data[:name], abbreviation: data[:abbreviation])
    end
    formatted_results
  end

  def self.federative_units
    total_federative_units = '27'
    if FederativeUnitRepository.new.count_all_fu != total_federative_units
      FederativeUnit.all_federative_units
    else
      database_federative_units = FederativeUnitRepository.new.all
      FederativeUnitsController.all_change_format(database_federative_units)
    end
  end

  def self.json_parse(response)
    JSON.parse(response.body, symbolize_names: true)
  end

  def self.save_federative_units(list)
    list.each do |fu|
      if FederativeUnitRepository.new.count_fu(fu[:id]) == '0'
        FederativeUnitRepository.new.save(fu[:id], fu[:nome], fu[:sigla])
      end
    end
  end
end
