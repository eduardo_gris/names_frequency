class CityRepository
  def initialize
    @connection = if ENV['ENVIRONMENT'] == 'test'
                    PG::Connection.new('host = db dbname = db-test user = postgres password = postgres')
                  else
                    PG::Connection.new('host = db dbname = db user = postgres password = postgres')
                  end
  end

  def save(city_id, name)
    @connection.exec "INSERT INTO cities (city_id, name)
                      VALUES(#{city_id}, '#{name.delete("'")}')"
    @connection.close
  end

  def count_city(city_id)
    result = @connection.exec "SELECT COUNT(city_id) FROM cities WHERE city_id='#{city_id}'"
    @connection.close
    result.getvalue(0, 0)
  end
end
