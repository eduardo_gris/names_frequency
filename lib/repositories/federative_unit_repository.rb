class FederativeUnitRepository
  def initialize
    @connection = if ENV['ENVIRONMENT'] == 'test'
                    PG::Connection.new('host = db dbname = db-test user = postgres password = postgres')
                  else
                    PG::Connection.new('host = db dbname = db user = postgres password = postgres')
                  end
  end

  def save(fu_id, name, abbreviation)
    @connection.exec "INSERT INTO federative_units (fu_id, name, abbreviation)
                      VALUES(#{fu_id}, '#{name}', '#{abbreviation}')"
    @connection.close
  end

  def count_fu(fu_id)
    result = @connection.exec "SELECT COUNT(fu_id) FROM federative_units WHERE fu_id='#{fu_id}'"
    @connection.close
    result.getvalue(0, 0)
  end

  def count_all_fu
    result = @connection.exec 'SELECT COUNT(fu_id) FROM federative_units'
    @connection.close
    result.getvalue(0, 0)
  end

  def all
    result = @connection.exec 'SELECT * FROM federative_units'
    @connection.close
    result
  end
end
