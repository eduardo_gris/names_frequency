class RankingNameRepository
  def initialize
    @connection = if ENV['ENVIRONMENT'] == 'test'
                    PG::Connection.new('host = db dbname = db-test user = postgres password = postgres')
                  else
                    PG::Connection.new('host = db dbname = db user = postgres password = postgres')
                  end
  end

  def save(rank_info)
    @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
                      VALUES('#{rank_info.sex}', #{rank_info.ranking},
                             '#{rank_info.name}', #{rank_info.frequency}, #{rank_info.local_id})"
    @connection.close
  end

  def update(rank_info)
    @connection.exec "UPDATE ranking_names SET name = '#{rank_info.name}', frequency = #{rank_info.frequency}
                      WHERE sex='#{rank_info.sex}' AND local_id=#{rank_info.local_id} AND ranking=#{rank_info.ranking}"
    @connection.close
  end

  def count_ranking(rank_name)
    result = @connection.exec "SELECT COUNT(ranking) FROM ranking_names WHERE sex='#{rank_name.sex}'
                               AND ranking=#{rank_name.ranking} AND local_id=#{rank_name.local_id}"
    @connection.close
    result.getvalue(0, 0)
  end

  def count_rankings_sex(sex, local_id)
    result = @connection.exec "SELECT COUNT(ranking) FROM ranking_names WHERE sex='#{sex}'
                               AND local_id=#{local_id}"
    @connection.close
    result.getvalue(0, 0)
  end

  def where(sex, local_id)
    result = @connection.exec "SELECT * FROM ranking_names WHERE sex='#{sex}'
                               AND local_id=#{local_id} ORDER BY
                               ranking ASC"
    @connection.close
    result
  end
end
