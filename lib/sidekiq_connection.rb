require 'bundler'
require 'sidekiq'
Bundler.require(:default)

Sidekiq.configure_server do |config|
  config.redis = { url: 'redis://redis:6379/0' }
end

Sidekiq.configure_client do |config|
  config.redis = { url: 'redis://redis:6379/0' }
end

PROJECT_ROOT = File.expand_path('..', __dir__)
Dir.glob(File.join(PROJECT_ROOT, 'lib', 'clients', '*.rb')).each { |file| require file }
Dir.glob(File.join(PROJECT_ROOT, 'lib', 'controllers', '*.rb')).each { |file| require file }
Dir.glob(File.join(PROJECT_ROOT, 'lib', 'jobs', '*.rb')).each { |file| require file }
Dir.glob(File.join(PROJECT_ROOT, 'lib', 'models', '*.rb')).each { |file| require file }
Dir.glob(File.join(PROJECT_ROOT, 'lib', 'repositories', '*.rb')).each { |file| require file }
Dir.glob(File.join(PROJECT_ROOT, 'lib', 'views', '**', '*.rb')).each { |file| require file }
