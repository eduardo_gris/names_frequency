class IbgeApiClient
  def self.federative_unit_api
    Faraday.get 'https://servicodados.ibge.gov.br/api/v1/localidades/estados'
  end

  def self.fu_cities_api(fu_id)
    Faraday.get "https://servicodados.ibge.gov.br/api/v1/localidades/estados/#{fu_id}/municipios"
  end

  def self.general_rank_names_api(local_id)
    Faraday.get "https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{local_id}"
  end

  def self.woman_rank_names_api(local_id)
    Faraday.get "https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{local_id}&&sexo=F"
  end

  def self.man_rank_names_api(local_id)
    Faraday.get "https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{local_id}&&sexo=M"
  end

  def self.decade_name_frequencies_api(name)
    Faraday.get "https://servicodados.ibge.gov.br/api/v2/censos/nomes/#{name}"
  end
end
