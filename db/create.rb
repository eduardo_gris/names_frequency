require 'pg'

begin
  connection = PG::Connection.new('host = db dbname = db-test user = postgres password = postgres')
  connection.exec 'CREATE TABLE federative_units(id SERIAL PRIMARY KEY,
                                                 fu_id INT, name VARCHAR, abbreviation VARCHAR(2))'
  connection.exec 'CREATE TABLE cities(id SERIAL PRIMARY KEY,
                                                 city_id INT, name VARCHAR)'
  connection.exec 'CREATE TABLE ranking_names(id SERIAL PRIMARY KEY,
                                                 created_date DATE DEFAULT CURRENT_DATE, sex VARCHAR(1), ranking INT,
                                                 name VARCHAR, frequency INT, local_id INT)'

  connection = PG::Connection.new('host = db dbname = db user = postgres password = postgres')
  connection.exec 'CREATE TABLE federative_units(id SERIAL PRIMARY KEY,
                                                 fu_id INT, name VARCHAR, abbreviation VARCHAR(2))'
  connection.exec 'CREATE TABLE cities(id SERIAL PRIMARY KEY,
                                                 city_id INT, name VARCHAR)'
  connection.exec 'CREATE TABLE ranking_names(id SERIAL PRIMARY KEY,
                                                 created_date DATE DEFAULT CURRENT_DATE, sex VARCHAR(1), ranking INT,
                                                 name VARCHAR, frequency INT, local_id INT)'
rescue PG::Error => e
  puts e.message
end
