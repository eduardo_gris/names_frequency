require 'pg'

@connection = PG::Connection.new('host = db dbname = db user = postgres password = postgres')
@connection.exec('TRUNCATE federative_units')
@connection.exec('TRUNCATE cities')
@connection.exec('TRUNCATE ranking_names')
