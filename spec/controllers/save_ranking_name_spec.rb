require 'spec_helper'

describe 'Save ranking names' do
  context '.general_create(id)' do
    it 'save general ranking names in database which was not recorded before' do
      data = []
      json_content = File.read('spec/support/apis/general_rank_names_city.json')
      faraday_response = double('general_rank_names', status: 200, body: json_content)
      city_id = 1300029

      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{city_id}")
        .and_return(faraday_response)
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', 1, 'MARIA', 682, 1300029)"

      RankingNamesController.general_create(city_id)
      results = @connection.exec 'SELECT * FROM ranking_names'
      results.each do |result|
        data << result.transform_keys!(&:to_sym)
      end

      expect(data[0][:sex]).to eq('G')
      expect(data[0][:ranking]).to eq('1')
      expect(data[0][:name]).to eq('MARIA')
      expect(data[0][:frequency]).to eq('682')
      expect(data[0][:local_id]).to eq('1300029')
      expect(data[1][:ranking]).not_to eq('1')
      expect(data[1][:name]).not_to eq('MARIA')
      expect(data[1][:frequency]).not_to eq('682')
      expect(data[1][:sex]).to eq('G')
      expect(data[1][:ranking]).to eq('2')
      expect(data[1][:name]).to eq('JOSE')
      expect(data[1][:frequency]).to eq('265')
      expect(data[1][:local_id]).to eq('1300029')
      expect(data[2][:sex]).to eq('G')
      expect(data[2][:ranking]).to eq('3')
      expect(data[2][:name]).to eq('RAIMUNDO')
      expect(data[2][:frequency]).to eq('207')
      expect(data[2][:local_id]).to eq('1300029')
    end
  end

  context '.woman_create(id)' do
    it 'save woman ranking names in database which was not recorded before' do
      data = []
      json_content = File.read('spec/support/apis/woman_rank_names_city.json')
      faraday_response = double('woman_rank_names', status: 200, body: json_content)
      city_id = 1300029

      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{city_id}&&sexo=F")
        .and_return(faraday_response)
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('W', '1', 'MARIA', '673', '1300029')"

      RankingNamesController.woman_create(city_id)
      results = @connection.exec 'SELECT * FROM ranking_names'
      results.each do |result|
        data << result.transform_keys!(&:to_sym)
      end

      expect(data[0][:sex]).to eq('W')
      expect(data[0][:ranking]).to eq('1')
      expect(data[0][:name]).to eq('MARIA')
      expect(data[0][:frequency]).to eq('673')
      expect(data[0][:local_id]).to eq('1300029')
      expect(data[1][:ranking]).not_to eq('1')
      expect(data[1][:name]).not_to eq('MARIA')
      expect(data[1][:frequency]).not_to eq('673')
      expect(data[1][:sex]).to eq('W')
      expect(data[1][:ranking]).to eq('2')
      expect(data[1][:name]).to eq('ANA')
      expect(data[1][:frequency]).to eq('133')
      expect(data[1][:local_id]).to eq('1300029')
      expect(data[2][:sex]).to eq('W')
      expect(data[2][:ranking]).to eq('3')
      expect(data[2][:name]).to eq('RAIMUNDA')
      expect(data[2][:frequency]).to eq('93')
      expect(data[2][:local_id]).to eq('1300029')
    end
  end

  context '.man_create(id)' do
    it 'save man ranking names in database which was not recorded before' do
      data = []
      json_content = File.read('spec/support/apis/man_rank_names_city.json')
      faraday_response = double('man_rank_names', status: 200, body: json_content)
      city_id = 1300029

      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{city_id}&&sexo=M")
        .and_return(faraday_response)
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('M', '1', 'JOSE', '263', '1300029')"

      RankingNamesController.man_create(city_id)
      results = @connection.exec 'SELECT * FROM ranking_names'
      results.each do |result|
        data << result.transform_keys!(&:to_sym)
      end

      expect(data[0][:sex]).to eq('M')
      expect(data[0][:ranking]).to eq('1')
      expect(data[0][:name]).to eq('JOSE')
      expect(data[0][:frequency]).to eq('263')
      expect(data[0][:local_id]).to eq('1300029')
      expect(data[1][:ranking]).not_to eq('1')
      expect(data[1][:name]).not_to eq('JOSE')
      expect(data[1][:frequency]).not_to eq('263')
      expect(data[1][:sex]).to eq('M')
      expect(data[1][:ranking]).to eq('2')
      expect(data[1][:name]).to eq('RAIMUNDO')
      expect(data[1][:frequency]).to eq('204')
      expect(data[1][:local_id]).to eq('1300029')
      expect(data[2][:sex]).to eq('M')
      expect(data[2][:ranking]).to eq('3')
      expect(data[2][:name]).to eq('FRANCISCO')
      expect(data[2][:frequency]).to eq('195')
      expect(data[2][:local_id]).to eq('1300029')
    end
  end
end
