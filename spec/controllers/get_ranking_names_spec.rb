require 'spec_helper'

describe 'Get all rank names' do
  context '.where_change_format' do
    it 'changes format of database saved ranking names' do
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', 1, 'MARIA', 682, 1300029)"
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', 2, 'PEDRO', 650, 1300029)"
      results = @connection.exec 'SELECT * FROM ranking_names'
      formatted_results = RankingNamesController.where_change_format(results)

      expect(formatted_results[0].sex).to eq 'G'
      expect(formatted_results[0].ranking).to eq '1'
      expect(formatted_results[0].name).to eq 'MARIA'
      expect(formatted_results[0].local_id).to eq '1300029'
      expect(formatted_results[1].sex).to eq 'G'
      expect(formatted_results[1].ranking).to eq '2'
      expect(formatted_results[1].name).to eq 'PEDRO'
      expect(formatted_results[1].local_id).to eq '1300029'
    end
  end

  context '.more_than_one_year(results)' do
    it 'returns true if the results are recorded more than one year before' do
      @connection.exec "INSERT INTO ranking_names (created_date, sex, ranking, name, frequency, local_id)
        VALUES('2019-05-29', 'G', 1, 'MARIA', 682, 1300029)"
      @connection.exec "INSERT INTO ranking_names (created_date, sex, ranking, name, frequency, local_id)
        VALUES('2019-05-29', 'G', 2, 'PEDRO', 650, 1300029)"
      results = @connection.exec 'SELECT * FROM ranking_names'

      expect(RankingNamesController.more_than_one_year?(results)).to eq true
    end

    it 'returns false if the results are recorded less than one year before' do
      @connection.exec "INSERT INTO ranking_names (created_date, sex, ranking, name, frequency, local_id)
        VALUES('2020-10-29', 'G', 1, 'MARIA', 682, 1300029)"
      @connection.exec "INSERT INTO ranking_names (created_date, sex, ranking, name, frequency, local_id)
        VALUES('2020-10-29', 'G', 2, 'PEDRO', 650, 1300029)"
      results = @connection.exec 'SELECT * FROM ranking_names'

      Timecop.freeze(2020, 11, 20) do
        expect(RankingNamesController.more_than_one_year?(results)).to eq false
      end
    end
  end

  context '.database_different_total_quantity?(sex, id)' do
    it 'returns true if database ranking quantity is different to total ranking quantity' do
      sex = 'G'
      id = 1300029
      allow(RankingNameRepository).to receive(:count_rankings_sex).and_return('10')

      expect(RankingNamesController.database_different_total_quantity?(sex, id)).to eq true
    end

    it 'returns false if database ranking quantity is equal to total ranking quantity' do
      sex = 'G'
      id = 1300029
      allow(RankingNameRepository).to receive(:count_rankings_sex).and_return('20')

      expect(RankingNamesController.database_different_total_quantity?(sex, id)).to eq true
    end
  end

  context '.general_rank_names(id)' do
    it 'call RankingName.general_rank_names if ranking names were not record on database (count != 20)' do
      general_rank_names1 = []
      general_rank_names2 = []
      id = 13
      general_rank_names1 << RankingName.new(sex: 'G', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 13)
      general_rank_names2 << RankingName.new(sex: 'G', ranking: 1, name: 'Maria', frequency: 682, local_id: 1300029)
      allow(RankingName).to receive(:general_rank_names).and_return(general_rank_names1)
      allow(RankingNameRepository).to receive(:where_change_format).and_return(general_rank_names2)

      expect { RankingNamesController.general_rank_names(id) }.to output(/Eduardo/).to_stdout
      expect { RankingNamesController.general_rank_names(id) }.not_to output(/Maria/).to_stdout
    end

    it 'call RankingNameRepository.where if all ranking names were record on database (count = 20)' do
      general_rank_names1 = []
      general_rank_names2 = []
      general_rank_names1 << RankingName.new(sex: 'G', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 13)
      general_rank_names2 << RankingName.new(sex: 'G', ranking: 1, name: 'Maria', frequency: 682, local_id: 1300029)
      i = 0
      id = 1300029
      while i < 20
        @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', #{1 + i}, 'MARIA', 682, 1300029)"
        i += 1
      end
      allow(RankingName).to receive(:general_rank_names).and_return(general_rank_names1)
      allow(RankingNamesController).to receive(:where_change_format).and_return(general_rank_names2)

      expect { RankingNamesController.general_rank_names(id) }.to output(/Maria/).to_stdout
      expect { RankingNamesController.general_rank_names(id) }.not_to output(/Eduardo/).to_stdout
    end

    it 'update general ranking names if all were record on database more than one year before (count = 20)' do
      i = 0
      id = 13
      while i < 20
        @connection.exec "INSERT INTO ranking_names (created_date, sex, ranking, name, frequency, local_id)
        VALUES('2017-10-10', 'G', #{1 + i}, 'MARIA', 682, 13)"
        i += 1
      end
      json_content = File.read('spec/support/apis/general_rank_names_uf.json')
      faraday_response = double('general_rank_names', status: 200, body: json_content)

      allow(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{id}")
        .and_return(faraday_response)

      expect { RankingNamesController.general_rank_names(id) }.to output(/MARIA/).to_stdout
      expect { RankingNamesController.general_rank_names(id) }.to output(/JOSE/).to_stdout
      expect { RankingNamesController.general_rank_names(id) }.to output(/FRANCISCO/).to_stdout
      expect { RankingNamesController.general_rank_names(id) }.to output(/173034/).to_stdout
      expect { RankingNamesController.general_rank_names(id) }.to output(/68995/).to_stdout
      expect { RankingNamesController.general_rank_names(id) }.to output(/47716/).to_stdout
    end

    it 'does not update general ranking names if all were record on database less than one year before (count = 20)' do
      i = 0
      id = 13
      while i < 20
        @connection.exec "INSERT INTO ranking_names (created_date, sex, ranking, name, frequency, local_id)
        VALUES('2020-10-10', 'G', #{1 + i}, 'ANA', 682, 13)"
        i += 1
      end
      json_content = File.read('spec/support/apis/general_rank_names_uf.json')
      faraday_response = double('general_rank_names', status: 200, body: json_content)

      allow(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{id}")
        .and_return(faraday_response)

      Timecop.freeze(2020, 11, 20) do
        expect { RankingNamesController.general_rank_names(id) }.not_to output(/MARIA/).to_stdout
        expect { RankingNamesController.general_rank_names(id) }.not_to output(/JOSE/).to_stdout
        expect { RankingNamesController.general_rank_names(id) }.not_to output(/FRANCISCO/).to_stdout
        expect { RankingNamesController.general_rank_names(id) }.not_to output(/173034/).to_stdout
        expect { RankingNamesController.general_rank_names(id) }.not_to output(/68995/).to_stdout
        expect { RankingNamesController.general_rank_names(id) }.not_to output(/47716/).to_stdout
      end
    end
  end

  context '.woman_rank_names(id)' do
    it 'call RankingName.woman_rank_names if ranking names were not record on database (count != 20)' do
      woman_rank_names1 = []
      woman_rank_names2 = []
      id = 13
      woman_rank_names1 << RankingName.new(sex: 'W', ranking: 1, name: 'Ana', frequency: 9900, local_id: 13)
      woman_rank_names2 << RankingName.new(sex: 'W', ranking: 1, name: 'Maria', frequency: 682, local_id: 1300029)
      allow(RankingName).to receive(:woman_rank_names).and_return(woman_rank_names1)
      allow(RankingNameRepository).to receive(:where_change_format).and_return(woman_rank_names2)

      expect { RankingNamesController.woman_rank_names(id) }.to output(/Ana/).to_stdout
      expect { RankingNamesController.woman_rank_names(id) }.not_to output(/Maria/).to_stdout
    end

    it 'call RankingNameRepository.where if all ranking names were record on database (count = 20)' do
      woman_rank_names1 = []
      woman_rank_names2 = []
      woman_rank_names1 << RankingName.new(sex: 'W', ranking: 1, name: 'Ana', frequency: 9900, local_id: 13)
      woman_rank_names2 << RankingName.new(sex: 'W', ranking: 1, name: 'Maria', frequency: 682, local_id: 1300029)
      i = 0
      id = 1300029
      while i < 20
        @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('W', #{1 + i}, 'MARIA', 682, 1300029)"
        i += 1
      end
      allow(RankingName).to receive(:woman_rank_names).and_return(woman_rank_names1)
      allow(RankingNamesController).to receive(:where_change_format).and_return(woman_rank_names2)

      expect { RankingNamesController.woman_rank_names(id) }.to output(/Maria/).to_stdout
      expect { RankingNamesController.woman_rank_names(id) }.not_to output(/Ana/).to_stdout
    end

    it 'update woman ranking names if all were record on database more than one year before (count = 20)' do
      i = 0
      id = 13
      while i < 20
        @connection.exec "INSERT INTO ranking_names (created_date, sex, ranking, name, frequency, local_id)
        VALUES('2017-10-10', 'W', #{1 + i}, 'MARIA', 682, 13)"
        i += 1
      end
      json_content = File.read('spec/support/apis/woman_rank_names_uf.json')
      faraday_response = double('woman_rank_names', status: 200, body: json_content)

      allow(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{id}&&sexo=F")
        .and_return(faraday_response)

      expect { RankingNamesController.woman_rank_names(id) }.to output(/MARIA/).to_stdout
      expect { RankingNamesController.woman_rank_names(id) }.to output(/ANA/).to_stdout
      expect { RankingNamesController.woman_rank_names(id) }.to output(/RAIMUNDA/).to_stdout
      expect { RankingNamesController.woman_rank_names(id) }.to output(/172183/).to_stdout
      expect { RankingNamesController.woman_rank_names(id) }.to output(/47399/).to_stdout
      expect { RankingNamesController.woman_rank_names(id) }.to output(/21790/).to_stdout
    end

    it 'does not update woman ranking names if all were record on database less than one year before (count = 20)' do
      i = 0
      id = 13
      while i < 20
        @connection.exec "INSERT INTO ranking_names (created_date, sex, ranking, name, frequency, local_id)
        VALUES('2020-10-10', 'W', #{1 + i}, 'ARLETE', 682, 13)"
        i += 1
      end
      json_content = File.read('spec/support/apis/woman_rank_names_uf.json')
      faraday_response = double('woman_rank_names', status: 200, body: json_content)

      allow(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{id}&&sexo=F")
        .and_return(faraday_response)

      Timecop.freeze(2020, 11, 20) do
        expect { RankingNamesController.woman_rank_names(id) }.not_to output(/MARIA/).to_stdout
        expect { RankingNamesController.woman_rank_names(id) }.not_to output(/ANA/).to_stdout
        expect { RankingNamesController.woman_rank_names(id) }.not_to output(/RAIMUNDA/).to_stdout
        expect { RankingNamesController.woman_rank_names(id) }.not_to output(/172183/).to_stdout
        expect { RankingNamesController.woman_rank_names(id) }.not_to output(/47399/).to_stdout
        expect { RankingNamesController.woman_rank_names(id) }.not_to output(/21790/).to_stdout
      end
    end
  end

  context '.man_rank_names(id)' do
    it 'call RankingName.man_rank_names if ranking names were not record on database (count != 20)' do
      man_rank_names1 = []
      man_rank_names2 = []
      id = 13
      man_rank_names1 << RankingName.new(sex: 'M', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 13)
      man_rank_names2 << RankingName.new(sex: 'M', ranking: 1, name: 'Pedro', frequency: 682, local_id: 1300029)
      allow(RankingName).to receive(:man_rank_names).and_return(man_rank_names1)
      allow(RankingNameRepository).to receive(:where_change_format).and_return(man_rank_names2)

      expect { RankingNamesController.man_rank_names(id) }.to output(/Eduardo/).to_stdout
      expect { RankingNamesController.man_rank_names(id) }.not_to output(/Pedro/).to_stdout
    end

    it 'call RankingNameRepository.where if all ranking names were record on database (count = 20)' do
      man_rank_names1 = []
      man_rank_names2 = []
      man_rank_names1 << RankingName.new(sex: 'M', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 13)
      man_rank_names2 << RankingName.new(sex: 'M', ranking: 1, name: 'Pedro', frequency: 682, local_id: 1300029)
      i = 0
      id = 1300029
      while i < 20
        @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('M', #{1 + i}, 'Pedro', 682, 1300029)"
        i += 1
      end
      allow(RankingName).to receive(:man_rank_names).and_return(man_rank_names1)
      allow(RankingNamesController).to receive(:where_change_format).and_return(man_rank_names2)

      expect { RankingNamesController.man_rank_names(id) }.to output(/Pedro/).to_stdout
      expect { RankingNamesController.man_rank_names(id) }.not_to output(/Eduardo/).to_stdout
    end

    it 'update man ranking names if all were record on database more than one year before (count = 20)' do
      i = 0
      id = 13
      while i < 20
        @connection.exec "INSERT INTO ranking_names (created_date, sex, ranking, name, frequency, local_id)
        VALUES('2017-10-10', 'M', #{1 + i}, 'PEDRO', 682, 13)"
        i += 1
      end
      json_content = File.read('spec/support/apis/man_rank_names_uf.json')
      faraday_response = double('man_rank_names', status: 200, body: json_content)

      allow(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{id}&&sexo=M")
        .and_return(faraday_response)

      expect { RankingNamesController.man_rank_names(id) }.to output(/JOSE/).to_stdout
      expect { RankingNamesController.man_rank_names(id) }.to output(/FRANCISCO/).to_stdout
      expect { RankingNamesController.man_rank_names(id) }.to output(/ANTONIO/).to_stdout
      expect { RankingNamesController.man_rank_names(id) }.to output(/68613/).to_stdout
      expect { RankingNamesController.man_rank_names(id) }.to output(/47493/).to_stdout
      expect { RankingNamesController.man_rank_names(id) }.to output(/42404/).to_stdout
    end

    it 'does not update man ranking names if all were record on database less than one year before (count = 20)' do
      i = 0
      id = 13
      while i < 20
        @connection.exec "INSERT INTO ranking_names (created_date, sex, ranking, name, frequency, local_id)
        VALUES('2020-10-10', 'M', #{1 + i}, 'PEDRO', 682, 13)"
        i += 1
      end
      json_content = File.read('spec/support/apis/man_rank_names_uf.json')
      faraday_response = double('man_rank_names', status: 200, body: json_content)

      allow(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{id}&&sexo=M")
        .and_return(faraday_response)

      Timecop.freeze(2020, 11, 20) do
        expect { RankingNamesController.man_rank_names(id) }.not_to output(/JOSE/).to_stdout
        expect { RankingNamesController.man_rank_names(id) }.not_to output(/FRANCISCO/).to_stdout
        expect { RankingNamesController.man_rank_names(id) }.not_to output(/ANTONIO/).to_stdout
        expect { RankingNamesController.man_rank_names(id) }.not_to output(/68613/).to_stdout
        expect { RankingNamesController.man_rank_names(id) }.not_to output(/47493/).to_stdout
        expect { RankingNamesController.man_rank_names(id) }.not_to output(/42404/).to_stdout
      end
    end
  end
end
