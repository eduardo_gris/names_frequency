require 'spec_helper'

describe 'Update ranking names' do
  context '.rank_update' do
    it 'update ranking names in database' do
      rank_names = []
      data = []
      id = 13
      sex = 'G'

      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', 1, 'AUGUSTO', 1000, 13)"
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', 2, 'JACYRA', 900, 13)"
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', 3, 'CLEIDE', 800, 13)"
      rank_names << RankingName.new(sex: 'G', ranking: 1, name: 'EDUARDO', frequency: 1300, local_id: 13)
      rank_names << RankingName.new(sex: 'G', ranking: 2, name: 'ANA', frequency: 1200, local_id: 13)
      rank_names << RankingName.new(sex: 'G', ranking: 3, name: 'JOAQUIM', frequency: 1100, local_id: 13)
      results = RankingNamesController.rank_update(rank_names, sex, id)
      results.each do |result|
        data << result.transform_keys!(&:to_sym)
      end

      expect(data[0][:sex]).to eq('G')
      expect(data[0][:ranking]).to eq('1')
      expect(data[0][:name]).to eq('EDUARDO')
      expect(data[0][:frequency]).to eq('1300')
      expect(data[0][:local_id]).to eq('13')
      expect(data[1][:sex]).to eq('G')
      expect(data[1][:ranking]).to eq('2')
      expect(data[1][:name]).to eq('ANA')
      expect(data[1][:frequency]).to eq('1200')
      expect(data[1][:local_id]).to eq('13')
      expect(data[2][:sex]).to eq('G')
      expect(data[2][:ranking]).to eq('3')
      expect(data[2][:name]).to eq('JOAQUIM')
      expect(data[2][:frequency]).to eq('1100')
      expect(data[2][:local_id]).to eq('13')
    end
  end

  context '.general_update(id)' do
    it 'update general ranking names in database' do
      data = []
      json_content = File.read('spec/support/apis/general_rank_names_uf.json')
      faraday_response = double('general_rank_names', status: 200, body: json_content)
      fu_id = 13

      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{fu_id}")
        .and_return(faraday_response)
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', 1, 'AUGUSTO', 1000, 13)"
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', 2, 'JACYRA', 900, 13)"
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', 3, 'CLEIDE', 800, 13)"

      RankingNamesController.general_update(fu_id)
      results = @connection.exec 'SELECT * FROM ranking_names'
      results.each do |result|
        data << result.transform_keys!(&:to_sym)
      end

      expect(data[0][:sex]).to eq('G')
      expect(data[0][:ranking]).to eq('1')
      expect(data[0][:name]).to eq('MARIA')
      expect(data[0][:frequency]).to eq('173034')
      expect(data[0][:local_id]).to eq('13')
      expect(data[1][:sex]).to eq('G')
      expect(data[1][:ranking]).to eq('2')
      expect(data[1][:name]).to eq('JOSE')
      expect(data[1][:frequency]).to eq('68995')
      expect(data[1][:local_id]).to eq('13')
      expect(data[2][:sex]).to eq('G')
      expect(data[2][:ranking]).to eq('3')
      expect(data[2][:name]).to eq('FRANCISCO')
      expect(data[2][:frequency]).to eq('47716')
      expect(data[2][:local_id]).to eq('13')
    end
  end

  context '.woman_update(id)' do
    it 'update woman ranking names in database' do
      data = []
      json_content = File.read('spec/support/apis/woman_rank_names_uf.json')
      faraday_response = double('woman_rank_names', status: 200, body: json_content)
      fu_id = 13

      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{fu_id}&&sexo=F")
        .and_return(faraday_response)
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('W', 1, 'JACYRA', 900, 13)"
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('W', 2, 'CLEIDE', 800, 13)"
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('W', 3, 'KARINA', 500, 13)"

      RankingNamesController.woman_update(fu_id)
      results = @connection.exec 'SELECT * FROM ranking_names'
      results.each do |result|
        data << result.transform_keys!(&:to_sym)
      end

      expect(data[0][:sex]).to eq('W')
      expect(data[0][:ranking]).to eq('1')
      expect(data[0][:name]).to eq('MARIA')
      expect(data[0][:frequency]).to eq('172183')
      expect(data[0][:local_id]).to eq('13')
      expect(data[1][:sex]).to eq('W')
      expect(data[1][:ranking]).to eq('2')
      expect(data[1][:name]).to eq('ANA')
      expect(data[1][:frequency]).to eq('47399')
      expect(data[1][:local_id]).to eq('13')
      expect(data[2][:sex]).to eq('W')
      expect(data[2][:ranking]).to eq('3')
      expect(data[2][:name]).to eq('RAIMUNDA')
      expect(data[2][:frequency]).to eq('21790')
      expect(data[2][:local_id]).to eq('13')
    end
  end

  context '.man_update(id)' do
    it 'update man ranking names in database' do
      data = []
      json_content = File.read('spec/support/apis/man_rank_names_uf.json')
      faraday_response = double('man_rank_names', status: 200, body: json_content)
      fu_id = 13

      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{fu_id}&&sexo=M")
        .and_return(faraday_response)
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('M', 1, 'AUGUSTO', 1000, 13)"
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('M', 2, 'JOBSON', 500, 13)"
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('M', 3, 'ALEXANDRE', 400, 13)"

      RankingNamesController.man_update(fu_id)
      results = @connection.exec 'SELECT * FROM ranking_names'
      results.each do |result|
        data << result.transform_keys!(&:to_sym)
      end

      expect(data[0][:sex]).to eq('M')
      expect(data[0][:ranking]).to eq('1')
      expect(data[0][:name]).to eq('JOSE')
      expect(data[0][:frequency]).to eq('68613')
      expect(data[0][:local_id]).to eq('13')
      expect(data[1][:sex]).to eq('M')
      expect(data[1][:ranking]).to eq('2')
      expect(data[1][:name]).to eq('FRANCISCO')
      expect(data[1][:frequency]).to eq('47493')
      expect(data[1][:local_id]).to eq('13')
      expect(data[2][:sex]).to eq('M')
      expect(data[2][:ranking]).to eq('3')
      expect(data[2][:name]).to eq('ANTONIO')
      expect(data[2][:frequency]).to eq('42404')
      expect(data[2][:local_id]).to eq('13')
    end
  end
end
