require 'spec_helper'

describe 'Get federative unit id' do
  context '.all_change_format' do
    it 'changes format of database saved federative units' do
      @connection.exec "INSERT INTO federative_units (fu_id, name, abbreviation)
        VALUES(13, 'Amazonas', 'AM')"
      @connection.exec "INSERT INTO federative_units (fu_id, name, abbreviation)
        VALUES(11, 'Rondônia', 'RO')"
      results = @connection.exec 'SELECT * FROM federative_units'
      formatted_results = FederativeUnitsController.all_change_format(results)

      expect(formatted_results[0].fu_id).to eq('13')
      expect(formatted_results[0].name).to eq('Amazonas')
      expect(formatted_results[0].abbreviation).to eq('AM')
      expect(formatted_results[1].fu_id).to eq('11')
      expect(formatted_results[1].name).to eq('Rondônia')
      expect(formatted_results[1].abbreviation).to eq('RO')
    end
  end
  context '.federative_units' do
    it 'call FederativeUnitsController.all_federative_units if not all federative units are saved in database' do
      federative_units1 = []
      federative_units1 << FederativeUnit.new(fu_id: 13, name: 'Amazonas', abbreviation: 'AM')

      expect(FederativeUnit).to receive(:all_federative_units).and_return(federative_units1)
      expect(FederativeUnitsController).not_to receive(:all_change_format)
      expect(FederativeUnitsController.federative_units).to eq(federative_units1)
    end
  end

  it 'call FederativeUnitsController.database_federative_units if all federative units are saved in database' do
    federative_units1 = []
    federative_units1 << FederativeUnit.new(fu_id: 11, name: 'Rondônia', abbreviation: 'RO')

    i = 0
    while i < 27
      @connection.exec "INSERT INTO federative_units (fu_id, name, abbreviation)
      VALUES(#{11 + i}, 'Amazonas', 'AM')"
      i += 1
    end

    expect(FederativeUnit).not_to receive(:all_federative_units)
    expect(FederativeUnitsController).to receive(:all_change_format).and_return(federative_units1)
    expect(FederativeUnitsController.federative_units).to eq(federative_units1)
  end

  context '.federative_unit_choose' do
    it 'get federative unit id by federative unit abbreviation' do
      json_content = File.read('spec/support/apis/list_fu.json')
      faraday_response = double('federative_units', status: 200, body: json_content)

      expect(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados')
                                      .and_return(faraday_response)
      expect(FederativeUnitsController).to receive(:puts)
      expect(FederativeUnitsController).to receive(:gets).and_return('AM')

      expect(FederativeUnitsController.federative_unit_choose).to eq(13)
    end

    it 'does not get federative unit id with incorrect federative unit abbreviation' do
      json_content = File.read('spec/support/apis/list_fu.json')
      faraday_response = double('federative_units', status: 200, body: json_content)

      expect(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados')
                                      .and_return(faraday_response)
      expect(FederativeUnitsController).to receive(:puts)
      expect(FederativeUnitsController).to receive(:gets).and_return('AMER')

      expect(FederativeUnitsController.federative_unit_choose).to eq('')
    end
  end
end
