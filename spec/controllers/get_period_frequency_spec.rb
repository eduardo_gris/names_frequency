require 'spec_helper'

describe 'Get period and frequency' do
  context '.names_choose_format' do
    it 'transform names separated by comma in array of names' do
      expect(NameFrequenciesController).to receive(:gets).and_return('Eduardo, Felipe, ,Ana')
      expect(NameFrequenciesController).to receive(:puts)

      expect(NameFrequenciesController.names_choose_format).to eq(['Eduardo', 'Felipe', 'Ana'])
    end
  end

  context '.names_frequency_info(names)' do
    it 'gets period and frequency of all correct names required' do
      names = ['Eduardo', 'Ana']
      json_content1 = File.read('spec/support/apis/decade_eduardo_frequency.json')
      faraday_response1 = double('decade_name_frequencies', status: 200, body: json_content1)
      json_content2 = File.read('spec/support/apis/decade_ana_frequency.json')
      faraday_response2 = double('decade_name_frequencies', status: 200, body: json_content2)

      allow(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/#{names[0]}")
        .and_return(faraday_response1)
      allow(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/#{names[1]}")
        .and_return(faraday_response2)
      names_frequency_info = NameFrequenciesController.names_frequency_info(names)

      expect(names_frequency_info[0][0].name).to eq('EDUARDO')
      expect(names_frequency_info[0][1].name).to eq('EDUARDO')
      expect(names_frequency_info[0][2].name).to eq('EDUARDO')
      expect(names_frequency_info[0][0].period).to eq('1930-1940')
      expect(names_frequency_info[0][1].period).to eq('1940-1950')
      expect(names_frequency_info[0][2].period).to eq('1950-1960')
      expect(names_frequency_info[0][0].frequency).to eq(5136)
      expect(names_frequency_info[0][1].frequency).to eq(11591)
      expect(names_frequency_info[0][2].frequency).to eq(25775)
      expect(names_frequency_info[1][0].name).to eq('ANA')
      expect(names_frequency_info[1][1].name).to eq('ANA')
      expect(names_frequency_info[1][2].name).to eq('ANA')
      expect(names_frequency_info[1][0].period).to eq('1930-1940')
      expect(names_frequency_info[1][1].period).to eq('1940-1950')
      expect(names_frequency_info[1][2].period).to eq('1950-1960')
      expect(names_frequency_info[1][0].frequency).to eq(56160)
      expect(names_frequency_info[1][1].frequency).to eq(101259)
      expect(names_frequency_info[1][2].frequency).to eq(183941)
    end
  end

  it 'does not get period and frequency of incorrect names required' do
    names = ['Edgardon', 'Ana']
    json_content1 = File.read('spec/support/apis/invalid_name.json')
    faraday_response1 = double('decade_name_frequencies', status: 200, body: json_content1)
    json_content2 = File.read('spec/support/apis/decade_ana_frequency.json')
    faraday_response2 = double('decade_name_frequencies', status: 200, body: json_content2)

    allow(Faraday).to receive(:get)
      .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/#{names[0]}")
      .and_return(faraday_response1)
    allow(Faraday).to receive(:get)
      .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/#{names[1]}")
      .and_return(faraday_response2)
    names_frequency_info = NameFrequenciesController.names_frequency_info(names)

    expect(names_frequency_info[0][0].name).to eq('ANA')
    expect(names_frequency_info[0][1].name).to eq('ANA')
    expect(names_frequency_info[0][2].name).to eq('ANA')
    expect(names_frequency_info[0][0].period).to eq('1930-1940')
    expect(names_frequency_info[0][1].period).to eq('1940-1950')
    expect(names_frequency_info[0][2].period).to eq('1950-1960')
    expect(names_frequency_info[0][0].frequency).to eq(56160)
    expect(names_frequency_info[0][1].frequency).to eq(101259)
    expect(names_frequency_info[0][2].frequency).to eq(183941)
  end
end
