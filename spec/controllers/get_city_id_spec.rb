require 'spec_helper'

describe 'Get city id' do
  context '.city_choose' do
    it 'get city id by federative unit id' do
      fu_id = 13
      json_content = File.read('spec/support/apis/list_fu_cities.json')
      faraday_response = double('cities', status: 200, body: json_content)

      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v1/localidades/estados/#{fu_id}/municipios")
        .and_return(faraday_response)
      expect(CitiesController).to receive(:puts)
      expect(CitiesController).to receive(:gets).and_return('Anamã')

      expect(CitiesController.city_choose(fu_id)).to eq(1300086)
    end

    it 'does not get city id with incorrect federative unit id' do
      fu_id = ''
      json_content = File.read('spec/support/apis/no_list_fu_cities.json')
      faraday_response = double('cities', status: 200, body: json_content)

      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v1/localidades/estados/#{fu_id}/municipios")
        .and_return(faraday_response)
      expect(CitiesController).to receive(:puts)
      expect(CitiesController).to receive(:gets).and_return('Anamã')

      expect(CitiesController.city_choose(fu_id)).to eq('')
    end

    it 'does not get city id with incorrect city name' do
      fu_id = 13
      json_content = File.read('spec/support/apis/list_fu_cities.json')
      faraday_response = double('cities', status: 200, body: json_content)

      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v1/localidades/estados/#{fu_id}/municipios")
        .and_return(faraday_response)
      expect(CitiesController).to receive(:puts)
      expect(CitiesController).to receive(:gets).and_return('Anamando')

      expect(CitiesController.city_choose(fu_id)).to eq('')
    end
  end
end
