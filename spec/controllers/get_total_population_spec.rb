require 'spec_helper'

describe 'Get total population' do
  context '.get_population(id)' do
    it 'gets the population by id' do
      id = 13

      expect(PopulationsController.get_total_population(id)).to eq 4144597
    end
  end

  context '.comma_separator(percentage)' do
    it 'change number separator to comma' do
      percentage = 4.1

      expect(PopulationsController.comma_separator(percentage)).to eq '4,1'
    end
  end

  context '.get_population_percentage(frequency, id)' do
    it 'gets population percentage by frequency and id' do
      id = 13
      frequency = 173034

      expect(PopulationsController.get_population_percentage(frequency, id)).to eq '4,2 %'
    end
  end
end
