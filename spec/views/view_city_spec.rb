require 'spec_helper'

describe 'View city' do
  context '.city_title' do
    it 'show message to write the city name' do
      expect(CityView.city_title)
        .to include('Digite o nome da cidade para ver o ranking dos nomes mais comuns:')
    end
  end
end
