require 'spec_helper'

describe 'View messages' do
  context '.success_table' do
    it 'show success message for ranking names table' do
      expect(Success.success_table).to include('Tabelas com ranking de nomes acima')
    end
  end

  context '.success_names_table' do
    it 'show success message for frequency names table' do
      expect(Success.success_names_table).to include('Tabela de frequência de nomes por década acima')
    end
  end

  context '.success_save' do
    it 'show success message for save city in database' do
      expect(Success.success_save).to include('salvo com sucesso')
    end
  end

  context '.incorrect_requisition' do
    it 'show error message for incorrect input requisition' do
      expect(Error.incorrect_requisition).to include('Requisição incorreta')
    end
  end

  context '.incorrect_option' do
    it 'show error message for incorrect menu option requisition' do
      expect(Error.incorrect_option).to include('Opção inválida')
    end
  end

  context '.saved_before' do
    it 'show error message for city saved before' do
      expect(Error.saved_before).to include('já está salvo')
    end
  end
end
