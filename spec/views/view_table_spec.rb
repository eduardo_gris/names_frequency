require 'spec_helper'

describe 'View table' do
  context '.title_table' do
    it 'show table title structure' do
      title_table = TableStructure.title_table

      expect(title_table).to include('----------------------------------------')
      expect(title_table).to include('|    Nome    |  Ranking   | Frequência |')
      expect(title_table).to include('----------------------------------------')
    end
  end

  context '.general_title' do
    it 'show table title of general rank names' do
      expect(GeneralTable.general_title).to include('RANK GERAL DOS NOMES MAIS COMUNS:')
    end
  end

  context '.woman_title' do
    it 'show table title of woman rank names' do
      expect(WomanTable.woman_title).to include('RANK DOS NOMES FEMININOS MAIS COMUNS:')
    end
  end

  context '.man_title' do
    it 'show table title of man rank names' do
      expect(ManTable.man_title).to include('RANK DOS NOMES MASCULINOS MAIS COMUNS:')
    end
  end

  context '.title_base' do
    it 'show required names base title' do
      decade_name_frequencies1 = []
      decade_name_frequencies2 = []
      names_frequency = []

      decade_name_frequencies1 << NameFrequency.new(name: 'EDUARDO', period: '1930-1940', frequency: 5136)
      decade_name_frequencies1 << NameFrequency.new(name: 'EDUARDO', period: '1940-1950', frequency: 11591)
      decade_name_frequencies2 << NameFrequency.new(name: 'ANA', period: '1930-1940', frequency: 56160)
      decade_name_frequencies2 << NameFrequency.new(name: 'ANA', period: '1940-1950', frequency: 101259)
      names_frequency << decade_name_frequencies1
      names_frequency << decade_name_frequencies2

      expect(NamesTable.title_base(names_frequency)).to eq('|PERÍODO/NOME|   EDUARDO  |     ANA    |')
    end
  end

  context '.names_title' do
    it 'show required names title' do
      decade_name_frequencies1 = []
      decade_name_frequencies2 = []
      names_frequency = []

      decade_name_frequencies1 << NameFrequency.new(name: 'EDUARDO', period: '1930-1940', frequency: 5136)
      decade_name_frequencies2 << NameFrequency.new(name: 'ANA', period: '1930-1940', frequency: 56160)
      names_frequency << decade_name_frequencies1
      names_frequency << decade_name_frequencies2
      names_title = ERB.new(NamesTable.names_title).result(binding)

      expect(names_title).to include('FREQUÊNCIA DE NOMES POR DÉCADA:')
      expect(names_title).to include('----------------------------------------')
      expect(names_title).to include('|PERÍODO/NOME|   EDUARDO  |     ANA    |')
      expect(names_title).to include('----------------------------------------')
    end
  end

  context '.names_content' do
    it 'show required names content' do
      decade_name_frequencies1 = []
      decade_name_frequencies2 = []
      names_frequency = []

      decade_name_frequencies1 << NameFrequency.new(name: 'EDUARDO', period: '1930-1940', frequency: 5136)
      decade_name_frequencies2 << NameFrequency.new(name: 'ANA', period: '1930-1940', frequency: 56160)
      names_frequency << decade_name_frequencies1
      names_frequency << decade_name_frequencies2
      names_content = ERB.new(NamesTable.names_content).result(binding)

      expect(names_content).to include('|  1930-1940 |    5136    |    56160   |')
      expect(names_content).to include('----------------------------------------')
    end
  end

  context '.result_view' do
    it 'structure the show of table content with result length negative' do
      length = 5
      result = 'Maria'

      expect(TableStructure.result_view(length, result)).to eq('    Maria   |')
    end

    it 'structure the show of table content with result length positive' do
      length = 4
      result = 'Guto'

      expect(TableStructure.result_view(length, result)).to eq('    Guto    |')
    end
  end

  context '.table_content' do
    it 'show table content line' do
      result = []
      result << RankingName.new(sex: 'G', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 12)

      expect(TableStructure.table_content(result[0])).to include('|   Eduardo  |      1     |    9900    |')
    end
  end

  context '.table_content_percentage(result)' do
    it 'show table content line with percentage included' do
      result = []
      result << RankingName.new(sex: 'G', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 13)

      expect(TableStructure.table_content_percentage(result[0]))
        .to include('|   Eduardo  |      1     |    9900    |    0,2 %   |')
    end
  end
end
