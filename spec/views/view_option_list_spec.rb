require 'spec_helper'

describe 'View option list' do
  context '.option_list' do
    it 'show all search options' do
      option_list = Option.option_list

      expect(option_list).to include('RANKING DOS NOMES MAIS COMUNS NO PAÍS')
      expect(option_list).to include('1')
      expect(option_list).to include('Pesquisar nomes mais comuns por unidade federativa')
      expect(option_list).to include('2')
      expect(option_list).to include('Pesquisar nomes mais comuns por cidade')
      expect(option_list).to include('3')
      expect(option_list).to include('Pesquisar frequência de nomes específicos por década')
      expect(option_list).to include('4')
      expect(option_list).to include('Salvar ranking de nomes no banco de dados')
      expect(option_list).to include('5')
      expect(option_list).to include('Sair')
      expect(option_list).to include('Escolha a opção de pesquisa:')
    end
  end
end
