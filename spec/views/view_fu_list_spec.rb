require 'spec_helper'

describe 'View federative units list' do
  context '.view_list' do
    it 'show list of all federative_units' do
      federative_units = []
      federative_units << FederativeUnit.new(fu_id: 13, name: 'Amazonas', abbreviation: 'AM')
      federative_units << FederativeUnit.new(fu_id: 21, name: 'Rio de Janeiro', abbreviation: 'RJ')
      view_list = ERB.new(FuList.view_list).result(binding)

      expect(view_list).to include('UNIDADES FEDERATIVAS:')
      expect(view_list).to include('Amazonas - AM')
      expect(view_list).to include('Rio de Janeiro - RJ')
      expect(view_list)
        .to include('Acima estão todas as unidades federativas com suas siglas.')
      expect(view_list)
        .to include('Digite a sigla da unidade federativa escolhida:')
    end
  end
end
