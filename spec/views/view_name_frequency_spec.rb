require 'spec_helper'

describe 'View name frequency' do
  context '.name_frequency_title' do
    it 'show message to write valid names' do
      name_frequency_title = NameFrequencyView.name_frequency_title

      expect(name_frequency_title)
        .to include('Digite nomes válidos para ver a frequência por década.')
      expect(name_frequency_title)
        .to include('Digite separado por vírgula:')
    end
  end
end
