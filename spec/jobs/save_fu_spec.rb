require 'spec_helper'

describe 'Save federative units' do
  context '.perform' do
    it 'save federative units in database which was not recorded before' do
      data = []
      json_content = File.read('spec/support/apis/list_fu.json')
      faraday_response = double('federative_units', status: 200, body: json_content)

      expect(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados')
                                      .and_return(faraday_response)
      @connection.exec "INSERT INTO federative_units (fu_id, name, abbreviation)
                      VALUES('11', 'Rondônia', 'RO')"
      FederativeUnitsJob.new.perform
      results = @connection.exec 'SELECT * FROM federative_units'
      results.each do |result|
        data << result.transform_keys!(&:to_sym)
      end

      expect(data[0][:fu_id]).to eq('11')
      expect(data[0][:name]).to eq('Rondônia')
      expect(data[0][:abbreviation]).to eq('RO')
      expect(data[1][:fu_id]).not_to eq('11')
      expect(data[1][:name]).not_to eq('Rondônia')
      expect(data[1][:abbreviation]).not_to eq('RO')
      expect(data[1][:fu_id]).to eq('12')
      expect(data[1][:name]).to eq('Acre')
      expect(data[1][:abbreviation]).to eq('AC')
      expect(data[2][:fu_id]).to eq('13')
      expect(data[2][:name]).to eq('Amazonas')
      expect(data[2][:abbreviation]).to eq('AM')
    end
  end

  context '.perform_async' do
    it 'starts async process to save federative units in database which was not recorded before' do
      expect { FederativeUnitsJob.perform_async }.to change(FederativeUnitsJob.jobs, :size).by(1)
      expect { FederativeUnitsJob.perform_async }.to change(FederativeUnitsJob.jobs, :size).by(1)
      expect(FederativeUnitsJob.jobs.size).to eq 2
    end
  end
end
