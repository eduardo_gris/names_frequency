require 'spec_helper'

describe 'Save cities' do
  context '.perform(city_id, name)' do
    it 'save cities in database which was not recorded before' do
      data = []
      city_id1 = 1300029
      name1 = 'Alvarães'
      city_id2 = 1300060
      name2 = 'Amaturá'
      @connection.exec "INSERT INTO cities (city_id, name)
                      VALUES('1300029', 'Alvarães')"
      CitiesJob.new.perform(city_id1, name1)
      CitiesJob.new.perform(city_id2, name2)
      results = @connection.exec 'SELECT * FROM cities'
      results.each do |result|
        data << result.transform_keys!(&:to_sym)
      end

      expect(data[0][:city_id]).to eq('1300029')
      expect(data[0][:name]).to eq('Alvarães')
      expect(data[1][:city_id]).not_to eq('1300029')
      expect(data[1][:name]).not_to eq('Alvarães')
      expect(data[1][:city_id]).to eq('1300060')
      expect(data[1][:name]).to eq('Amaturá')
    end
  end

  context '.perform_async(city_id, name)' do
    it 'starts async process to save cities and ranking names in database which was not recorded before' do
      city_id1 = 1300029
      name1 = 'Alvarães'
      city_id2 = 1300060
      name2 = 'Amaturá'

      expect { CitiesJob.perform_async(city_id1, name1) }.to change(CitiesJob.jobs, :size).by(1)
      expect { CitiesJob.perform_async(city_id2, name2) }.to change(CitiesJob.jobs, :size).by(1)
      expect(CitiesJob.jobs.size).to eq 2
    end
  end
end
