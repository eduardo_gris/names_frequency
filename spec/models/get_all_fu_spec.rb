require 'spec_helper'

describe 'Get all federative units' do
  context '.all_federative_units' do
    it 'get all federative units by api' do
      json_content = File.read('spec/support/apis/list_fu.json')
      faraday_response = double('federative_units', status: 200, body: json_content)
      expect(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados')
                                      .and_return(faraday_response)
      all_federative_units = FederativeUnit.all_federative_units

      expect(all_federative_units[0].name).to eq('Rondônia')
      expect(all_federative_units[1].name).to eq('Acre')
      expect(all_federative_units[2].name).to eq('Amazonas')
      expect(all_federative_units[0].fu_id).to eq(11)
      expect(all_federative_units[1].fu_id).to eq(12)
      expect(all_federative_units[2].fu_id).to eq(13)
      expect(all_federative_units[0].abbreviation).to eq('RO')
      expect(all_federative_units[1].abbreviation).to eq('AC')
      expect(all_federative_units[2].abbreviation).to eq('AM')
    end
  end
end
