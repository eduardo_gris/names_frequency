require 'spec_helper'

describe 'Decade name frequency' do
  context '.period_format(period)' do
    it 'adjust decades period format' do
      period = '[1930,1940['

      expect(NameFrequency.period_format(period)).to eq('1930-1940')
    end
  end

  context '.all_decade_name_frequencies' do
    it 'get all decade name frequencies by name requisition' do
      choose_name = 'Eduardo'
      json_content = File.read('spec/support/apis/decade_eduardo_frequency.json')
      faraday_response = double('decade_name_frequencies', status: 200, body: json_content)

      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/#{choose_name}")
        .and_return(faraday_response)
      all_decade_name_frequencies = NameFrequency.all_decade_name_frequencies(choose_name)

      expect(all_decade_name_frequencies[0].name).to eq('EDUARDO')
      expect(all_decade_name_frequencies[1].name).to eq('EDUARDO')
      expect(all_decade_name_frequencies[2].name).to eq('EDUARDO')
      expect(all_decade_name_frequencies[0].period).to eq('1930-1940')
      expect(all_decade_name_frequencies[1].period).to eq('1940-1950')
      expect(all_decade_name_frequencies[2].period).to eq('1950-1960')
      expect(all_decade_name_frequencies[0].frequency).to eq(5136)
      expect(all_decade_name_frequencies[1].frequency).to eq(11591)
      expect(all_decade_name_frequencies[2].frequency).to eq(25775)
    end

    it 'does not get decade name frequencies by invalid name requisition' do
      choose_name = 'Edgardon'
      json_content = File.read('spec/support/apis/invalid_name.json')
      faraday_response = double('decade_name_frequencies', status: 200, body: json_content)

      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/#{choose_name}")
        .and_return(faraday_response)

      expect(NameFrequency.all_decade_name_frequencies(choose_name)).to eq([])
    end
  end
end
