require 'spec_helper'

describe 'Ranking names by federative unit' do
  context '.general_rank_names' do
    it 'get general ranking, names and frequency by federative unit id' do
      json_content = File.read('spec/support/apis/general_rank_names_uf.json')
      faraday_response = double('general_rank_names', status: 200, body: json_content)
      fu_id = 13
      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{fu_id}")
        .and_return(faraday_response)
      general_rank_names = RankingName.general_rank_names(fu_id)

      expect(general_rank_names[0].ranking).to eq(1)
      expect(general_rank_names[1].ranking).to eq(2)
      expect(general_rank_names[2].ranking).to eq(3)
      expect(general_rank_names[0].name).to eq('MARIA')
      expect(general_rank_names[1].name).to eq('JOSE')
      expect(general_rank_names[2].name).to eq('FRANCISCO')
      expect(general_rank_names[0].frequency).to eq(173034)
      expect(general_rank_names[1].frequency).to eq(68995)
      expect(general_rank_names[2].frequency).to eq(47716)
    end
  end

  context '.woman_rank_names' do
    it 'get woman ranking, names and frequency by federative unit id' do
      json_content = File.read('spec/support/apis/woman_rank_names_uf.json')
      faraday_response = double('woman_rank_names', status: 200, body: json_content)
      fu_id = 13
      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{fu_id}&&sexo=F")
        .and_return(faraday_response)
      woman_rank_names = RankingName.woman_rank_names(fu_id)

      expect(woman_rank_names[0].ranking).to eq(1)
      expect(woman_rank_names[1].ranking).to eq(2)
      expect(woman_rank_names[2].ranking).to eq(3)
      expect(woman_rank_names[0].name).to eq('MARIA')
      expect(woman_rank_names[1].name).to eq('ANA')
      expect(woman_rank_names[2].name).to eq('RAIMUNDA')
      expect(woman_rank_names[0].frequency).to eq(172183)
      expect(woman_rank_names[1].frequency).to eq(47399)
      expect(woman_rank_names[2].frequency).to eq(21790)
    end
  end

  context '.man_rank_names' do
    it 'get man ranking, names and frequency by federative unit id' do
      json_content = File.read('spec/support/apis/man_rank_names_uf.json')
      faraday_response = double('man_rank_names', status: 200, body: json_content)
      fu_id = 13
      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{fu_id}&&sexo=M")
        .and_return(faraday_response)
      man_rank_names = RankingName.man_rank_names(fu_id)

      expect(man_rank_names[0].ranking).to eq(1)
      expect(man_rank_names[1].ranking).to eq(2)
      expect(man_rank_names[2].ranking).to eq(3)
      expect(man_rank_names[0].name).to eq('JOSE')
      expect(man_rank_names[1].name).to eq('FRANCISCO')
      expect(man_rank_names[2].name).to eq('ANTONIO')
      expect(man_rank_names[0].frequency).to eq(68613)
      expect(man_rank_names[1].frequency).to eq(47493)
      expect(man_rank_names[2].frequency).to eq(42404)
    end
  end
end
