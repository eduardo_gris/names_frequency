require 'spec_helper'

describe 'Get federative unit cities' do
  context '.all_uf_cities' do
    it 'get all cities of specific federal unit' do
      json_content = File.read('spec/support/apis/list_fu_cities.json')
      faraday_response = double('cities', status: 200, body: json_content)
      fu_id = 13
      expect(Faraday).to receive(:get)
        .with("https://servicodados.ibge.gov.br/api/v1/localidades/estados/#{fu_id}/municipios")
        .and_return(faraday_response)
      all_uf_cities = City.all_uf_cities(fu_id)

      expect(all_uf_cities[0].name).to eq('Alvarães')
      expect(all_uf_cities[1].name).to eq('Amaturá')
      expect(all_uf_cities[2].name).to eq('Anamã')
      expect(all_uf_cities[0].city_id).to eq(1300029)
      expect(all_uf_cities[1].city_id).to eq(1300060)
      expect(all_uf_cities[2].city_id).to eq(1300086)
    end
  end
end
