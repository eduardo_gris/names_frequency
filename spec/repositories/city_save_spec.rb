require 'spec_helper'

describe 'City save' do
  context '.count_city(city_id)' do
    it 'count if city is saved in database' do
      city_id = 1300029
      name = 'Alvarães'
      CityRepository.new.save(city_id, name)

      expect(CityRepository.new.count_city(city_id)).to eq '1'
    end

    it 'does not count if city is not saved in database' do
      city_id = 1300029

      expect(CityRepository.new.count_city(city_id)).to eq '0'
    end
  end

  context '.save(city_id, name)' do
    it 'save city in database' do
      city_id = 1300029
      name = 'Alvarães'

      CityRepository.new.save(city_id, name)
      result = @connection.exec 'SELECT * FROM cities'
      data = result[0].transform_keys!(&:to_sym)

      expect(data[:city_id]).to eq('1300029')
      expect(data[:name]).to eq('Alvarães')
    end
  end
end
