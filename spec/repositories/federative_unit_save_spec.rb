require 'spec_helper'

describe 'Federative unit save' do
  context '.count_fu(fu_id)' do
    it 'count if federative unit is saved in database' do
      fu_id = 13
      name = 'Amazonas'
      abbreviation = 'AM'
      FederativeUnitRepository.new.save(fu_id, name, abbreviation)

      expect(FederativeUnitRepository.new.count_fu(fu_id)).to eq '1'
    end

    it 'does not count if federative unit is not saved in database' do
      fu_id = 13

      expect(FederativeUnitRepository.new.count_fu(fu_id)).to eq '0'
    end
  end

  context '.count_all_fu' do
    it 'count all federative unit save in database' do
      @connection.exec "INSERT INTO federative_units (fu_id, name, abbreviation)
        VALUES(13, 'Amazonas', 'AM')"
      @connection.exec "INSERT INTO federative_units (fu_id, name, abbreviation)
        VALUES(11, 'Rondônia', 'RO')"

      expect(FederativeUnitRepository.new.count_all_fu).to eq '2'
    end
  end

  context '.save(federative_unit)' do
    it 'save federative unit in database' do
      fu_id = 13
      name = 'Amazonas'
      abbreviation = 'AM'

      FederativeUnitRepository.new.save(fu_id, name, abbreviation)
      result = @connection.exec 'SELECT * FROM federative_units'
      data = result[0].transform_keys!(&:to_sym)

      expect(data[:fu_id]).to eq('13')
      expect(data[:name]).to eq('Amazonas')
      expect(data[:abbreviation]).to eq('AM')
    end
  end

  context '.all' do
    it 'get all federative units in database' do
      data = []
      @connection.exec "INSERT INTO federative_units (fu_id, name, abbreviation)
        VALUES(13, 'Amazonas', 'AM')"
      @connection.exec "INSERT INTO federative_units (fu_id, name, abbreviation)
        VALUES(11, 'Rondônia', 'RO')"

      results = FederativeUnitRepository.new.all
      results.each do |result|
        data << result.transform_keys!(&:to_sym)
      end

      expect(data[0][:fu_id]).to eq('13')
      expect(data[0][:name]).to eq('Amazonas')
      expect(data[0][:abbreviation]).to eq('AM')
      expect(data[1][:fu_id]).to eq('11')
      expect(data[1][:name]).to eq('Rondônia')
      expect(data[1][:abbreviation]).to eq('RO')
    end
  end
end
