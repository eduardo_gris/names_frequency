require 'spec_helper'

describe 'Ranking names repository' do
  context '.count_ranking(ranking_name)' do
    it 'count if ranking name is saved in database based in sex, ranking and local_id, for individual analyze' do
      general_rank_names1 = RankingName.new(sex: 'G', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 12)
      general_rank_names2 = RankingName.new(sex: 'M', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 12)
      general_rank_names3 = RankingName.new(sex: 'G', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 32)
      RankingNameRepository.new.save(general_rank_names1)
      RankingNameRepository.new.save(general_rank_names2)
      RankingNameRepository.new.save(general_rank_names3)

      expect(RankingNameRepository.new.count_ranking(general_rank_names1)).to eq '1'
    end

    it 'does not count if ranking name is not saved in database' do
      general_rank_names = RankingName.new(sex: 'G', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 12)

      expect(RankingNameRepository.new.count_ranking(general_rank_names)).to eq '0'
    end
  end

  context '.count_rankings_sex(sex, local_id)' do
    it 'count if ranking name is saved in database based in sex and local_id, for group analyze' do
      sex = 'G'
      local_id = 12
      general_rank_names1 = RankingName.new(sex: 'G', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 12)
      general_rank_names2 = RankingName.new(sex: 'M', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 32)
      general_rank_names3 = RankingName.new(sex: 'G', ranking: 2, name: 'Felipe', frequency: 8900, local_id: 12)
      RankingNameRepository.new.save(general_rank_names1)
      RankingNameRepository.new.save(general_rank_names2)
      RankingNameRepository.new.save(general_rank_names3)

      expect(RankingNameRepository.new.count_rankings_sex(sex, local_id)).to eq '2'
    end

    it 'does not count if ranking name is not saved in database' do
      sex = 'G'
      local_id = 12
      RankingName.new(sex: 'G', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 12)

      expect(RankingNameRepository.new.count_rankings_sex(sex, local_id)).to eq '0'
    end
  end

  context '.save(rank_info)' do
    it 'save ranking names in database' do
      general_rank_names = RankingName.new(sex: 'G', ranking: 1, name: 'Eduardo', frequency: 9900, local_id: 12)

      RankingNameRepository.new.save(general_rank_names)
      result = @connection.exec 'SELECT * FROM ranking_names'
      data = result[0].transform_keys!(&:to_sym)

      expect(data[:sex]).to eq('G')
      expect(data[:ranking]).to eq('1')
      expect(data[:name]).to eq('Eduardo')
      expect(data[:frequency]).to eq('9900')
      expect(data[:local_id]).to eq('12')
    end
  end

  context '.update(rank_info)' do
    it 'update ranking names in database' do
      general_rank_names = []
      data = []
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', 4, 'MARIA', 682, 20)"
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('M', 1, 'PERDIGÃO', 692, 20)"
      general_rank_names << RankingName.new(sex: 'G', ranking: 4, name: 'Pedro', frequency: 680, local_id: 20)
      general_rank_names << RankingName.new(sex: 'M', ranking: 1, name: 'Joaquim', frequency: 690, local_id: 20)

      RankingNameRepository.new.update(general_rank_names[0])
      RankingNameRepository.new.update(general_rank_names[1])
      results = @connection.exec 'SELECT * FROM ranking_names'
      results.each do |result|
        data << result.transform_keys!(&:to_sym)
      end

      expect(data[0][:sex]).to eq('G')
      expect(data[0][:ranking]).to eq('4')
      expect(data[0][:name]).to eq('Pedro')
      expect(data[0][:frequency]).to eq('680')
      expect(data[0][:local_id]).to eq('20')
      expect(data[1][:sex]).to eq('M')
      expect(data[1][:ranking]).to eq('1')
      expect(data[1][:name]).to eq('Joaquim')
      expect(data[1][:frequency]).to eq('690')
      expect(data[1][:local_id]).to eq('20')
    end
  end

  context '.where(sex, local_id)' do
    it 'get all ranking names saved in database, based in requested sex and local_id' do
      data = []
      sex = 'G'
      local_id = 20
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', 4, 'MARIA', 682, 20)"
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('M', 1, 'PERDIGÃO', 692, 20)"
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', 1, 'ANA', 1000, 22)"
      @connection.exec "INSERT INTO ranking_names (sex, ranking, name, frequency, local_id)
        VALUES('G', 5, 'PEDRO', 632, 20)"

      results = RankingNameRepository.new.where(sex, local_id)
      results.each do |result|
        data << result.transform_keys!(&:to_sym)
      end

      expect(data[0][:sex]).to eq('G')
      expect(data[0][:ranking]).to eq('4')
      expect(data[0][:name]).to eq('MARIA')
      expect(data[0][:frequency]).to eq('682')
      expect(data[0][:local_id]).to eq('20')
      expect(data[1][:sex]).not_to eq('M')
      expect(data[1][:ranking]).not_to eq('1')
      expect(data[1][:name]).not_to eq('PERDIGÃO')
      expect(data[1][:frequency]).not_to eq('692')
      expect(data[1][:ranking]).not_to eq('1')
      expect(data[1][:name]).not_to eq('ANA')
      expect(data[1][:frequency]).not_to eq('1000')
      expect(data[1][:local_id]).not_to eq('22')
      expect(data[1][:sex]).to eq('G')
      expect(data[1][:ranking]).to eq('5')
      expect(data[1][:name]).to eq('PEDRO')
      expect(data[1][:frequency]).to eq('632')
      expect(data[1][:local_id]).to eq('20')
    end
  end
end
