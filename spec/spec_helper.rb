require 'sidekiq/testing'
Sidekiq::Testing.fake!

require 'bundler'
Bundler.require(:default)

require_relative 'setup_test_database'

ENV['ENVIRONMENT'] = 'test'

PROJECT_ROOT = File.expand_path('..', __dir__)

Dir.glob(File.join(PROJECT_ROOT, 'lib', '**', '*.rb')).each do |file|
  autoload File.basename(file, '.rb').camelize, file
end

RSpec.configure do |config|
  config.before(:each) do
    setup_test_database!
  end
end
