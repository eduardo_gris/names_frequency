require 'pg'

def setup_test_database!
  @connection = PG::Connection.new('host = db dbname = db-test user = postgres password = postgres')
  @connection.exec('TRUNCATE federative_units')
  @connection.exec('TRUNCATE cities')
  @connection.exec('TRUNCATE ranking_names')
end
